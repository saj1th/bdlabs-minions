package main

import (
	"flag"

	"github.com/Sirupsen/logrus"
	"github.com/saj1th/envtoflag"
	"github.com/jrallison/go-workers"
	"bitbucket.org/saj1th/bdlabs-minions/app"
)

var (
	logr    *logrus.Logger
	appName string
)

func init() {
	logr = logrus.New()
	appName = "forecaster.minions"
}

func main() {
	var (
		mode       string
		redis_host string
		errors         []string

	)

	flag.StringVar(&mode, "mode", "dev", "dev|debug|prod")
	flag.StringVar(&redis_host, "redis-host", "", "redis.host")
	envtoflag.Parse(appName)
	app.PrintWelcome()
	if redis_host == "" {
		errors = append(errors, "redis-host")
	}

	app.ParseErrors(errors)

	logr.Level = app.GetLogrMode(mode)


	workers.Configure(map[string]string{
		"server":   redis_host, // location of redis instance
		"database": "0",                    // instance of the database
		"pool":     "30",                   // number of connections to keep open with redis
		"process":  "1",                    // unique process id for this instance of workers (for proper recovery of inprogress jobs on crash)
	})

	// Add job with concurrency 1
	workers.Process("forecaster", submitToSpark, 1)

	//Start stats server available via http://ip:port/stats
	go workers.StatsServer(5696)

	// Blocks until process is told to exit via unix signal
	workers.Run()

}

func submitToSpark(message *workers.Msg) {
	// Upload file to HDFS
	// Curl Request to Job server
	// Update Cassandra Status
	// Enqueue another job to query job server and update cassandra with job status

}

type FMiddleware struct{}

func (r *FMiddleware) Call(queue string, message *workers.Msg, next func() bool) (acknowledge bool) {
	//// Check log level and add logging
	acknowledge = next()
	return
}

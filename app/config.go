package app


// Envs
const (
	MODE_DEV    string = "dev"
	MODE_PROD   string = "prod"
	MODE_DEBUG  string = "debug"
	COOKIE_NAME string = "FORECASTER"
)
